
	const express = require ('express');
	const app = express();
	const port = 3000;


	app.use(express.json());
	app.use(express.urlencoded({extended:true}));




	app.get("/home", (req,res) =>{
		res.send("Welcome to the Homepage!");
	});



// Mock database

	let users = [
	  {
	    username: "johndoe",
	    password: "jdoe1234",
	  },
	];


	app.get("/users", (req, res) => {
	  res.send(users);
	});



	app.delete("/delete-user", (req,res) =>{
	
	let message;
	for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			message = `User ${req.body.username} has been deleted.`;
			break;

		}else{
			message = "User does not exist.";
		}
	}
		res.send(message)
	});


	app.listen(port, () => console.log(`Server running at port ${port}`));




